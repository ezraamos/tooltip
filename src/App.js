import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './components/NavBar'
import Sidebar from './components/Sidebar'

function App() {
  return (
    <div className="App">
      <NavBar/>
      <Sidebar/>
    </div>
  );
}

export default App;
