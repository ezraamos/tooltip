import React, { useState } from 'react';
import { Tooltip } from 'reactstrap';
import { FaHome, FaShoppingCart } from "react-icons/fa";
import { AiFillContacts } from "react-icons/ai";

export default function Sidebar(){
     
  const [homeOpen, setHomeOpen] = useState(false);
  const [isHomeHover, setIsHomeHover] = useState(false);
  const [isSideHomeHover, setIsSideHomeHover] = useState(false);

  const [shopOpen, setShopOpen] = useState(false);
  const [isShopHover, setIsShopHover] = useState(false);
  const [isSideShopHover, setIsSideShopHover] = useState(false);

  const [contactOpen, setContactOpen] = useState(false);
  const [isContactHover, setIsContactHover] = useState(false);
  const [isSideContactHover, setIsSideContactHover] = useState(false);

  const toggle1 = () => {
    setHomeOpen(!homeOpen) 
  };

  const toggle2 = () => {
    setShopOpen(!shopOpen) 
  };

  const toggle3 = () => {
    setContactOpen(!contactOpen) 
  };


    return(
        <div class="sidebar">
         <span className={isHomeHover === true || isSideHomeHover === true ? "sidebarHomeHovered" : "sidebarHome"} 
               id="Home"
               onMouseEnter={() => setIsSideHomeHover(true)}
               onMouseLeave={() => setIsSideHomeHover(false)}>  
            <FaHome/> 
            <Tooltip placement="right" isOpen={homeOpen} autohide={false} target="Home" toggle={toggle1}>
                <div className="toolTipItems"     
                     onMouseEnter={() => setIsHomeHover(true)}
                     onMouseLeave={() => setIsHomeHover(false)}>
                 <a href="/">Products</a>
                 <hr className="tooltip-horizontal-line"/>
                 <a href="/">Linesheets</a>
                 <hr className="tooltip-horizontal-line"/>
                 <a href="/">Categories</a>
                </div>
            </Tooltip>
         </span>
         <span className={isShopHover === true || isSideShopHover === true ? "sidebarShopHovered" : "sidebarShop"} 
               id="Shop"
               onMouseEnter={() => setIsSideShopHover(true)}
               onMouseLeave={() => setIsSideShopHover(false)}> 
              <FaShoppingCart/> 
              <Tooltip placement="right" isOpen={shopOpen} autohide={false} target="Shop" toggle={toggle2}>
                <div className="toolTipItems"
                     onMouseEnter={() => setIsShopHover(true)}
                     onMouseLeave={() => setIsShopHover(false)}>
                 <a href="/">Products</a>
                 <hr className="tooltip-horizontal-line"/>
                 <a href="/">Linesheets</a>
                 <hr className="tooltip-horizontal-line"/>
                 <a href="/">Categories</a>
                </div>
              </Tooltip>
         </span>
         <span className={isContactHover === true || isSideContactHover === true ? "sidebarContactHovered" : "sidebarContact"} 
               id="Contact"
               onMouseEnter={() => setIsSideContactHover(true)}
               onMouseLeave={() => setIsSideContactHover(false)}> 
            <AiFillContacts /> 
              <Tooltip placement="right" isOpen={contactOpen} autohide={false} target="Contact" toggle={toggle3}>
                <div className="toolTipItems"
                     onMouseEnter={() => setIsContactHover(true)}
                     onMouseLeave={() => setIsContactHover(false)}>
                 <a href="/">Products</a>
                 <hr className="tooltip-horizontal-line"/>
                 <a href="/">Linesheets</a>
                 <hr className="tooltip-horizontal-line"/>
                 <a href="/">Categories</a>
                </div>
          </Tooltip>
         </span>
        </div>
    )  
}




