import React from 'react';
import { FaPowerOff } from "react-icons/fa";
import brand from './brand2.png'
import kannan from './kannan.png'

import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem
} from 'reactstrap';

export default function NavBar(){
	return(
		<div>
			<Navbar className="navbar" color="white" light expand="md" fixed="top">
	          <NavbarBrand className="brand-logo" href="/"><img src={brand} alt="brand"/></NavbarBrand>
	          <Nav className="ml-auto d-flex align-items-center" navbar>
	            <NavItem className="emailLink">
	             kannan@test.com
	            </NavItem>
	            <NavItem className="profile-avatar">
	             <img className="avatar" src={kannan} alt="kannan"/>
	            </NavItem>
	            <NavItem className="powerOff">
	              <FaPowerOff/>
	            </NavItem>
	          </Nav>
	     	</Navbar>
	    </div>
	)
}